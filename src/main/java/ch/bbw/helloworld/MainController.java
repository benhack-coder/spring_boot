package ch.bbw.helloworld;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import javax.websocket.server.PathParam;

@Controller
public class MainController {

    @GetMapping("/{name}")
    public String home(Model model, @PathVariable("Name") String name) {
        model.addAttribute("message",  name != null ?  "Hello " + name : "Hello");
        return "index";
    }
}
